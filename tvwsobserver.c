// TODO:
// 1.- correct: /dev/ttyUSB0: Too many open files.
// running the code as it is with the run_tvwsobs.sh will randomly crash after hours of collection.
// Try to close the open fd descriptor and reopen for reading.
// Speed may be of consideration. May be open several files and then reuse the descriptors.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdlib.h>
#define BAUDRATE B500000
#define FALSE 0
#define TRUE 1
// small window for maxhold
#define MAXHOLD 4
// sampling time in secs
#define TOTAL_SAMPLE_TIME_SECS 5

int fd;
FILE *f;
char fname[21];
char *sweep_data;
time_t t;
time_t t_finish;
struct tm *localt;
struct tm *localt_f;
char buf[255];
char sPortName[255];
// char fileName[255];

// to pass to the handler
char shortFN[10];
long lowfreq, highfreq;

long stepfreq;

struct timeval tv;
struct timezone tz;

void open_rfexplorer(char *port);
void signal_handler_io (int status);
void set_scale(char *argv[]);
void get_sample(float samples[], unsigned *size);
void reset_max_hold(float max_hold[], int size);
void calc_max_hold(float samples[], int size, float max_hold[]);
void print_max_hold(float samples[], float max_hold[], int size, int sample_number);
void set_params(char *buf);
void print_sample(float samples[], int size, int low, int high);
int elapsed_sample_time(int);

int main(int argc, char **argv)
{
  
  float samples[200];
  float max_hold[200];
  int size, i, j;
  int total_secs_s;
    
  // calculate timestamp
  t = time(0);
  localt = localtime(&t);
  total_secs_s = localt->tm_hour*3600 + localt->tm_min*60 + localt->tm_sec;
 
  // calculate interval
  lowfreq=atol(argv[2]);
  highfreq=atol(argv[3]);
  int sample_number = atol(argv[7]); 
    
  open_rfexplorer(argv[1]);
  // set the RFExplorer frequency scale
  set_scale(argv);
  
  // the very first sample
  get_sample(samples, &size);
  reset_max_hold(max_hold, size);
  calc_max_hold(samples, size,  max_hold);
  
  j=0;

  // maxhold

  for (; elapsed_sample_time(total_secs_s) < TOTAL_SAMPLE_TIME_SECS;)
    {
      get_sample(samples, &size);
        
      //print_sample(samples, size, lowfreq, highfreq);
      j++;

/*
    // Alternative code
 
    if (j%5 == 0)
	{
      set_params("# C2-F:0300000,0350000,-120,-050");
	  usleep(20000);
	  get_sample(samples, &size);
	  print_sample(samples, size, 300000, 350000);
	  printf("zoom end\n");
      set_scale(argv);
      usleep(200000);
	  j=0;
	}
*/
      calc_max_hold(samples, size,  max_hold);
    }
//  printf("max hold\n");
  print_max_hold(samples, max_hold, size, sample_number);

 return 0;
}

int elapsed_sample_time(int total_secs_s) 
{

  time_t t_finish=time(0);
  struct tm * localt_f = localtime(&t_finish);

  int total_secs_f = localt_f->tm_hour*3600 + localt_f->tm_min*60 + localt_f->tm_sec;

  if (localt_f->tm_mday - localt->tm_mday == 0) 
  {
	return total_secs_f - total_secs_s;
  } 
  else
  {
	return 3600*24 - total_secs_s + total_secs_f;
  }

}

void signal_handler_IO (int status)
{
    // emtpy
}

void print_max_hold(float samples[], float max_hold[], int size, int sample_number)
{
  unsigned freq;
  int i;

  gettimeofday(&tv, &tz);
  t=tv.tv_sec+(tv.tv_usec/1000000.0);

  for (i=0; i<size; i++)
    {
      freq=(lowfreq+i*((highfreq-lowfreq)/size));
      //  printf("%u\t%5.2f\t%5.2f\n", freq, samples[i], max_hold[i]);
     // printf("%ul,%u,%5.2f\n", tv.tv_sec,freq, max_hold[i]);
      printf("%u %u %5.2f\n", sample_number,freq, max_hold[i]);
    }
  printf("\n");
}

void print_sample(float samples[], int size, int low, int high)
{
  unsigned freq;
  int i;

  double t;

  for (i=0; i<size; i++)
    {
      gettimeofday(&tv, &tz);
      freq=(low+i*((high-low)/size));
      t=tv.tv_sec+(tv.tv_usec/1000000.0);
      printf("%u %5.2f %.6lf\n", freq, samples[i], t);
    }
}


void reset_max_hold(float max_hold[], int size)
{
  int i;

  for (i=0; i<size; i++)
    {
      // a minimum value of -200 dBm
      max_hold[i]=-200;
    }
}

void calc_max_hold(float samples[], int size, float max_hold[])
{
  int i;

  for (i=0; i<size; i++)
    {
      if (samples[i]>max_hold[i])
          max_hold[i]=samples[i];
    }  
}

void get_sample(float samples[], unsigned *size)
{
  int res;
  int i;

  do 
    {
      memset(buf,0x0,sizeof(buf));
      //pause();
      res = read(fd,buf,255);
      //printf("(%c)",buf[0]); 
    } while(buf[0]!='$');
    

  //printf("--%c--",buf[0]); 
  
  *size=buf[2];

  // once received the signal the whole data should be available

  pause(); 

    
  res=0;
  while(res < *size + 5) 
    {
      res += read(fd,buf+res,255);
    }
  
  //printf("res = %i, size = %u\n",res,*size);



  for(i=0; i < *size; i++)
    {
      // be careful it starts at 3 not at 4 as before
      // since the string is $SXyyy where every y corresponds
      // to a different power value.

      samples[i]=((unsigned char*)buf)[i+3]/-2.0;
      
      // printf("%5.2f\t",samples[i]);
    }
}

void open_rfexplorer(char *port)
{
    // get port name
    strcpy(sPortName, port);
    
    fd = open(sPortName, O_RDWR | O_NOCTTY | O_NONBLOCK);
    
    if (fd <0)
    {
        perror(sPortName);
        exit(-1); 
    }
}

void set_params(char *buf)
{
  struct sigaction saio;
  struct termios oldtio, newtio;
  
  // the port is supossed to be already open.
  if (fd <0)
    {
        perror(sPortName);
        exit(-1);
    }

 /* install the signal handler before making the device asynchronous */
 memset(&saio,0x0,sizeof(saio));
 saio.sa_handler = signal_handler_IO;
 sigaction(SIGIO,&saio,NULL);
 
 /* allow the process to receive SIGIO */
 fcntl(fd, F_SETOWN, getpid());
 
 /* Make the file descriptor asynchronous (the manual page says only 
    O_APPEND and O_NONBLOCK, will work with F_SETFL...) */

 fcntl(fd, F_SETFL, FASYNC);

 tcgetattr(fd,&oldtio); /* save current port settings */

 newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
 newtio.c_iflag = IGNPAR | ICRNL;
 newtio.c_oflag = 0;
 newtio.c_lflag = ICANON;
 newtio.c_cc[VMIN]=1;
 newtio.c_cc[VTIME]=0;
 tcflush(fd, TCIFLUSH);
 tcsetattr(fd,TCSAFLUSH,&newtio);
   
 write(fd,buf,32); 

}


void set_scale(char *argv [])
{
  struct sigaction saio;
  struct termios oldtio, newtio;

  // port is already open
    
 if (fd <0)
 {
     perror(sPortName); 
     exit(-1); 
 }

 /* install the signal handler before making the device asynchronous */
 memset(&saio,0x0,sizeof(saio));
 saio.sa_handler = signal_handler_IO;
 sigaction(SIGIO,&saio,NULL);
 
 /* allow the process to receive SIGIO */
 fcntl(fd, F_SETOWN, getpid());
 
 /* Make the file descriptor asynchronous (the manual page says only 
    O_APPEND and O_NONBLOCK, will work with F_SETFL...) */
 fcntl(fd, F_SETFL, FASYNC);

 tcgetattr(fd,&oldtio); /* save current port settings */
 /* set new port settings for canonical input processing */
 newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
 newtio.c_iflag = IGNPAR | ICRNL;
 newtio.c_oflag = 0;
 newtio.c_lflag = ICANON;
 newtio.c_cc[VMIN]=1;
 newtio.c_cc[VTIME]=0;
 tcflush(fd, TCIFLUSH);
 tcsetattr(fd,TCSAFLUSH,&newtio);
 buf[0]='#';
 buf[1]=32;
 buf[2]='C';
 buf[3]='2';
 buf[4]='-';
 bufargv='F';
 buf[6]=':';
 buf[7]=argv[2][0];
 buf[8]=argv[2][1];
 buf[9]=argv[2][2];
 buf[10]=argv[2][3];
 buf[11]=argv[2][4];
 buf[12]=argv[2][5];
 buf[13]=argv[2][6];
 buf[14]=',';
 buf[15]=argv[3][0];
 buf[16]=argv[3][1];
 buf[17]=argv[3][2];
 buf[18]=argv[3][3];
 buf[19]=argv[3][4];
 buf[20]=argv[3][5];
 buf[21]=argv[3][6];
 buf[22]=',';
 buf[23]='-';
 buf[24]=argv[4][0];
 buf[25]=argv[4][1];
 buf[26]=argv[4][2];
 buf[27]=',';
 buf[28]='-';
 buf[29]=argv[5][0];
 buf[30]=argv[5][1];
 buf[31]=argv[5][2];
 
 write(fd,buf,32);


// strcpy(fileName, "001");  
// write(fd,fileName,3);
}

/*  strftime(fname,21,"%Y_%m_%d_%H_%M.txt",localt); */

// strftime(fname,4,argv[6],localt);


// EXTRA CODE:
/*  fprintf(f,"--------------------\r\n");
 strftime(line2,20,"%Y-%m-%d-%H:%M:%S",localt);
 fprintf(f,"%s\r\n",line2);
 fprintf(f,"\r\n");
 fprintf(f,"\r\n");
 fprintf(f,"Frequency(mHZ)\tSignal (dBm)\r\n");
*/  

// STORE IN A FILE
/*
f = fopen(argv[6],"w"); 

for(i=0;i<sweep_steps;i++)
{
 fprintf(f,"%ld\t%6.6f\r\n",(atol(argv[2])+i*(atol(argv[3])-atol(argv[2]))/sweep_steps)*1000,-1*(((float)(unsigned char)buf[i+4]))/2);

}

fclose(f);
*/

