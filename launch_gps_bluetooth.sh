#!/bin/bash

# test phone
# gps_mac_address="38:0A:94:B7:10:5D"

#channel=$(sdptool browse $gps_mac_address | tail -2 | awk '{print $2}')
channel=$(sdptool browse 78:59:5E:E4:9B:07 | grep -A 7 GPS | tail -n 1 | awk '{print $2}')

#channel=$(sdptool browse 38:0A:94:B7:10:5D | grep -A 7 GPS | tail -n 1 | awk '{print $2}')

echo $channel

sudo rm /etc/bluetooth/rfcomm.conf
echo "rfcomm0 {" > /etc/bluetooth/rfcomm.conf
echo "bind yes;" >> /etc/bluetooth/rfcomm.conf
echo "device  78:59:5E:E4:9B:07;" >> /etc/bluetooth/rfcomm.conf
echo "channel $channel;" >> /etc/bluetooth/rfcomm.conf
echo "comment \"Android GPS\";" >> /etc/bluetooth/rfcomm.conf
echo "}" >> /etc/bluetooth/rfcomm.conf

sudo rfcomm unbind rfcomm0
sudo rfcomm bind rfcomm0
sudo killall gpsd
sudo rm /var/run/gpsd.sock
sudo gpsd -b -D3 -N -F /var/run/gpsd.sock /dev/rfcomm0

