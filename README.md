**Read samples from RF Explorer**

This program collects samples from the RF Explorer.

Define before use:

| setting | description |
| --- | --- |
| MAXHOLD | small window to calculate maxhold within longer time period |
| TOTAL_SAMPLE_TIME_SECS | total sampling period in secs |

Usage:

```
$initfreq should contain 7 digits, e.g., 0130000 for 130MHz
$endfreq (idem)
$maxpower set the max scale of Y axis of the RFExplorer, should contain 3 digits, e.g., 050 
$minpower set the min scale of Y axis of the RFExplorer, should contain 3 digits, e.g., 120

tvwsobserver $USBFORRF $initfreq $endfreq $maxpower $minpower $filename
```
---

**Example in bash:**

```bash
USBFORRF=/dev/ttyUSB$(dmesg | grep cp210x | tail -1 |  sed -e 's/\(^.*\)\(.$\)/\2/')

#buckets of 5 secs during 2 hr = 1440 to observe the 868 band. 

for i in {1..1440}
do
./tvwsobserver $USBFORRF 0867000 0870000 050 120 test $i >> 867-870.csv 
done 
```

