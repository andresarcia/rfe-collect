CC=gcc
CFLAGS=-I. 

tvwsobserver: tvwsobserver.c
	$(CC)  -o tvwsobserver tvwsobserver.c $(CFLAGS) 

clean: 
	rm -f *.o
	rm tvwsobserver
