#!/bin/bash

#TODO:
#1-
#Figure out when the bluetooth leasing ends.
#since it doesnt respond to any command (sdptool)
#So as to requiere a new reconnection with the following command:
#hcitool cc 78:59:5E:E4:9B:07
#


function clean_channel {
#see if the BLUETOOTH is UP in the client device
while [[ $(sdptool browse $BTDEVICE | grep down) ]]
do
echo "Bluetooth interface is off -- Turn it on"
aplay $TVWSPATH/beep-5.wav -q
sleep 1
done

#see if the GPS service is UP
while [[ $(sdptool get --bdaddr $BTDEVICE $SERVICE | grep failed) ]]
do
echo "GPS Service is off -- Turn it on"
aplay $TVWSPATH/beep-5.wav -q
sleep 1
done

while [[ -z $(lsusb -d 0x10c4:0xea60) ]]
do
echo "Connect the Stpectrum Analyzer"
aplay $TVWSPATH/connectSS.wav -q
sleep 1
done 

}

#GT-7100 Samsumg Andres' phone.
BTDEVICE=78:59:5E:E4:9B:07
#Marco's phone.
#BTDEVICE=38:0A:94:B7:10:5D

#Service Record Number for ShareGPS
#SERVICE=0x0001000a
#SERVICEID=ShareGPS

#Minimum number of packets to get a TPV record
RECORDS=4
#Maximum number of packets to get a TPV record (depends on the Android APP)
#ex: BT2GPS needed MAX=11 and after v4 needs MAX=15
MAXRECORDS=15

#Service Record Number for GPS (as in GPS2BT APP)
SERVICE=0x0001000a
SERVICEID=GPS2BT

#Service Record Number for GPS (BlueShare)
#SERVICE=0x0001000b
#SERVICEID=BlueShare
#program directory
TVWSPATH=/home/pi/TVWS


# Marco: print the current dir
#current_dir=$(pwd)
#echo $current_dir > /home/pi/TVWS/current_dir.txt 

#cd /home/pi/TVWS/data/400
#/home/pi/TVWS/rfexplorer /dev/ttyUSB0 0400000 0500000 050 120 1.txt

#if [[ $RFE -eq "0" ]]; then
#mv 1.txt $TVWSPATH/data/400/1.txt
#fi


#Look for the lastest registered file
if [ "$(ls -A $TVWSPATH/data/400)" ]; then
latest_file=$(ls -tr $TVWSPATH/data/400 | sort -n | tail -n 1)
echo "Latest measure"
echo $latest_file
count=$(echo ${latest_file%.*})
else
echo "$TVWSPATH/data/400 is Empty"
count=1
fi

clean_channel 

if [ "$(rfcomm show $BTDEVICE 2>&1 | grep No)" ]; then
echo "First-time binding"
#obtain the EXPECTED channel from SDP local table
CHANNEL=$(sdptool browse $BTDEVICE | grep -A 7 $SERVICEID | tail -n 1 | awk '{print $2}')
echo "Desired Channel: $CHANNEL"

#sudo rfcomm unbind /dev/rfcomm0
sudo rfcomm release /dev/rfcomm0

while [[ "$(sudo rfcomm bind /dev/rfcomm0 $BTDEVICE $CHANNEL 2>&1 | grep Can\'t)" ]]
do
echo "Restarting gpsd (first time)"
sudo /etc/init.d/gpsd restart
sudo rfcomm bind /dev/rfcomm0 $BTDEVICE $CHANNEL
done
fi 

while :
do

# ANDRES:
# sleeps are trying to deal with a possible BT congestion problem.
# other possible causes:
# - try a different bluetooth device (apparently hama is problematic)
# - gpsd protocol is not straight forward to follow
# - see the order of reinitialization of every device 


if [ "$(rfcomm | grep closed)" ]; then
echo "Channel changed"
#echo "gpsd restarted BEFORE release-bind"
# HERE BEFORE: sudo /etc/init.d/gpsd restart
# Signal de GPSD to relaunch with the new expected CHANNEL
CHANNEL=$(sdptool browse $BTDEVICE | grep -A 7 $SERVICEID  | tail -n 1 | awk '{print $2}')
#BEFORE: sudo rfcomm unbind /dev/rfcomm0
sudo rfcomm release /dev/rfcomm0
sudo rfcomm bind /dev/rfcomm0 $BTDEVICE $CHANNEL

#read -p "Launch GPSD (y/n)? "
#[[ "$REPLY" == "y" ]] && sudo /etc/init.d/gpsd restart

echo "gpsd restarted after release-bind"
sudo /etc/init.d/gpsd restart
fi


if [ "$(rfcomm | grep connected)" ] || [ "$(rfcomm | grep clean)" ]; then

echo "RECORDS: $RECORDS"


# Added a timeout of 15 secs because from time to time the pipe to gpsd hangs.
# So gpsd has to be reset
# The previous solution was to reset the bluetooth conection at the phone
# 
ACTIVATED=$(timeout 8 "gpspipe" "-w" "-n" "3" | grep activated)

if [ "$ACTIVATED" ]; then
TPV=$(timeout 5 "gpspipe" "-w" "-n" "$RECORDS")
   if [ "$TPV" ]; then
      TPV=$(echo $TPV | grep -m 1 TPV)
   else
      echo "gpspipe is timing out!"
   fi
else
TPV=""
timeout 4 aplay "$TVWSPATH/nogps.wav" "-q"
echo "GPS deamon not ACTIVATED: waiting for the gpsd to wakeup"
sudo /etc/init.d/gpsd restart
fi

latitude=$(echo $TPV | sed -e 's/[{}]/''/g' | awk -v RS=',"' -F: '/^lat/ {print $2}')
longitude=$(echo $TPV | sed -e 's/[{}]/''/g' | awk -v RS=',"' -F: '/^lon/ {print $2}')

fi

# After it was missing longitud line to execute on 20/5/13
# I had to add an extra checking.

if [ -z "$latitude" ] || [ -z "$longitude" ]
then
echo "Missing Lat or Lon, please check the code!"
fi



# Measure spectrum
if [ ! -z "$latitude" ] && [ ! -z "$longitude" ]
then
aplay $TVWSPATH/beep-8.wav -q
#aplay $TVWSPATH/beep-7.wav -q

if [ "$RECORDS" -gt 4 ]; then 
RECORDS=`expr $RECORDS - 1`
fi

echo "MEASUMENT NUMBER: $count"

dmeas=$(date)
echo $dmeas
echo "LATITUDE"
echo $latitude
echo "LONGITUDE"
echo $longitude

cd $TVWSPATH/data/400
RFE=$(/home/pi/TVWS/rfexplorer /dev/ttyUSB0 0400000 0500000 050 120 $count.txt)

if [[ $RFE -eq "0" ]]; then
echo $latitude >> $TVWSPATH/data/400/$count.txt
echo $longitude >> $TVWSPATH/data/400/$count.txt
echo $dmeas >> $TVWSPATH/data/400/$count.txt
count=`expr $count + 1`
else
aplay $TVWSPATH/telephone-ring-3.wav -q
fi

else
 if [ "$RECORDS" -lt $MAXRECORDS ]; then
 RECORDS=`expr $RECORDS + 1`
 fi
fi

done
